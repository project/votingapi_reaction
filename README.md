# Voting API Reaction

## Introduction

A flexible field based on [Voting API](https://www.drupal.org/project/votingapi)
module, which allows users to react in a certain way to any entity. Just like on
Disqus, Facebook, Twitter, or Instagram.

## Requirements

In the version module dev (7) we use anonymize IP address data. It is necessary
to meet EU (GDPR) data protection regulations.

## Installation

1. Download and enable this module and it's dependency
   [Voting API](https://www.drupal.org/project/votingapi) module.
1. Create a field for the entity you'd like this feature on.

## Configuration

This module provides a number of settings to be as flexible as possible:

### Field settings

Each field has settings, that allows you to hide/show any reaction and select
how anonymous users are treated.

### Field formatter settings

Each field has formatter settings, that allows you to hide/show reaction summary
, label, icon or count. Also there is an option to sort reactions by count.

### Field value

When adding/editing an entity, you have the option to enable, disable or hide
reactions, specifically for that entity. Just like you do with comments.

### Permissions

On the permissions page, you can give permission to view, add or modify reaction
to any role.

## Customization options

### Vote Type

[Voting API](https://www.drupal.org/project/votingapi) module creates a page,
where you can manage Vote Types. You can use that page to create and manage your
own reactions, where you can use your own icons.

### Template file

Copy votingapi-reaction-item.html.twig from module folder to your theme folder
and customize the layout and CSS to suit your website design.


## MAINTAINERS

Current Maintainers:
- Elaman Imashov - [Elaman Imashov](https://www.drupal.org/u/elaman)
