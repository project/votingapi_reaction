<?php

namespace Drupal\votingapi_reaction;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ExtensionPathResolver;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Session\AccountProxy;
use Drupal\file\Entity\File;
use Drupal\votingapi\Entity\Vote;
use Drupal\votingapi\Entity\VoteType;
use Drupal\votingapi\VoteResultFunctionManager;
use Drupal\votingapi_reaction\Plugin\Field\FieldType\VotingApiReactionItemInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Manages reactions through Voting API entities.
 */
class VotingApiReactionManager implements ContainerInjectionInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Current user service.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  /**
   * Voting API results service.
   *
   * @var \Drupal\votingapi\VoteResultFunctionManager
   */
  protected $votingApiResults;

  /**
   * Renderer service.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * Configuration factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Path resolver service.
   *
   * @var \Drupal\Core\Extension\ExtensionPathResolver
   */
  protected $pathResolver;

  /**
   * File URL generator service.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;

  /**
   * Default reactions shipped with the module.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  public const DEFAULT_REACTIONS = [
    "reaction_angry" => "I'm angry",
    "reaction_laughing" => "I'm laughing",
    "reaction_like" => "I like this",
    "reaction_love" => "I love this",
    "reaction_sad" => "I'm sad",
    "reaction_surprised" => "I'm surprised",
  ];

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager service.
   * @param \Drupal\Core\Session\AccountProxy $currentUser
   *   Current user service.
   * @param \Drupal\votingapi\VoteResultFunctionManager $votingApiResults
   *   Voting API results service.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   Renderer service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Configuration factory service.
   * @param \Drupal\Core\Extension\ExtensionPathResolver $path_resolver
   *   Path resolver service.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $file_url_generator
   *   File URL generator service.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    AccountProxy $currentUser,
    VoteResultFunctionManager $votingApiResults,
    Renderer $renderer,
    ConfigFactoryInterface $configFactory,
    ExtensionPathResolver $path_resolver,
    FileUrlGeneratorInterface $file_url_generator,
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->currentUser = $currentUser;
    $this->votingApiResults = $votingApiResults;
    $this->renderer = $renderer;
    $this->configFactory = $configFactory;
    $this->pathResolver = $path_resolver;
    $this->fileUrlGenerator = $file_url_generator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('current_user'),
      $container->get('plugin.manager.votingapi.resultfunction'),
      $container->get('renderer'),
      $container->get('config.factory'),
      $container->get('extension.path.resolver'),
      $container->get('file_url_generator'),
    );
  }

  /**
   * Load previous reaction of the user for certain field.
   *
   * @param \Drupal\votingapi\Entity\Vote $entity
   *   Current vote entity.
   * @param array $settings
   *   Field settings.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Last reaction for current user.
   */
  public function lastReaction(Vote $entity, array $settings) {
    $voteStorage = $this->entityTypeManager->getStorage('vote');
    $query = $voteStorage->getQuery()
      ->accessCheck()
      ->condition('entity_id', $entity->getVotedEntityId())
      ->condition('entity_type', $entity->getVotedEntityType())
      ->condition('field_name', $entity->get('field_name')->value)
      ->condition('user_id', $this->currentUser->id());

    if ($this->currentUser->isAnonymous()) {
      // Filter by Cookie method.
      if (in_array(VotingApiReactionItemInterface::BY_COOKIES, $settings['anonymous_detection'])) {
        $recallReaction = $this->recallReaction($entity);
        // Only Cookie method used and no reaction for entity.
        if (
          !in_array(VotingApiReactionItemInterface::BY_IP, $settings['anonymous_detection']) &&
          is_null($recallReaction)
        ) {
          return NULL;
        }
        $query->condition('id', $recallReaction);
      }
      // Filter by IP method.
      if (in_array(VotingApiReactionItemInterface::BY_IP, $settings['anonymous_detection'])) {
        $query->condition('vote_source', Vote::getCurrentIp());
      }

      // Filter by rollover.
      $rollover = $settings['anonymous_rollover'];
      if ($rollover == VotingApiReactionItemInterface::VOTINGAPI_ROLLOVER) {
        $rollover = $this->configFactory
          ->get('votingapi.settings')
          ->get('anonymous_window');
      }
      if ($rollover != VotingApiReactionItemInterface::NEVER_ROLLOVER) {
        $query->condition('timestamp', time() - $rollover, '>=');
      }
    }

    $ids = $query->execute();

    return $voteStorage->load(intval(array_pop($ids)));
  }

  /**
   * Return voting results for each active reaction.
   *
   * @param \Drupal\votingapi\Entity\Vote $entity
   *   Current vote entity.
   * @param array $settings
   *   Field settings.
   *
   * @return array
   *   Array containing Voting API voting results.
   */
  public function getResults(Vote $entity, array $settings) {
    // Get results for each reaction.
    $results = $this->votingApiResults->getResults($entity->getVotedEntityType(), $entity->getVotedEntityId());
    $reactions = array_filter($settings['reactions']);

    return array_intersect_key($results, $reactions);
  }

  /**
   * Recalculate results for given reaction and entity.
   *
   * @param string $entity_type
   *   Voted entity type.
   * @param string $entity_id
   *   Voted entity id.
   * @param string $type
   *   Vote type.
   */
  public function recalculateResults($entity_type, $entity_id, $type) {
    $this->votingApiResults->recalculateResults($entity_type, $entity_id, $type);
  }

  /**
   * Return all vote types marked as reaction.
   *
   * @return array
   *   Active reaction vote types.
   */
  public function allReactions() {
    $voteTypeStorage = $this->entityTypeManager->getStorage('vote_type');
    return array_filter($voteTypeStorage->loadMultiple(), function (VoteType $entity) {
      return $entity->getThirdPartySetting('votingapi_reaction', 'reaction');
    });
  }

  /**
   * Return rendered list of active reactions.
   *
   * @param array $settings
   *   Field settings.
   * @param array $results
   *   Array containing Voting API voting results.
   *
   * @return array
   *   Rendered reactions.
   */
  public function getReactions(array $settings, array $results) {
    // Get only enabled reactions.
    $reactions = array_filter($this->allReactions(), function (VoteType $vote_type) use ($settings) {
      return $settings['reactions'][$vote_type->id()]['show'] ?? TRUE;
    });

    uasort($reactions, function ($a, $b) use ($settings, $results) {
      if ($settings['sort_reactions'] === 'none') {
        // Sort order defined by field settings.
        $weight_a = $settings['reactions'][$a->id()]['weight'] ?? 0;
        $weight_b = $settings['reactions'][$b->id()]['weight'] ?? 0;

        return $weight_a - $weight_b;
      }
      else {
        // Sort order by number of reactions.
        $count_a = $results[$a->id()]['vote_count'] ?? 0;
        $count_b = $results[$b->id()]['vote_count'] ?? 0;
        $asc = $count_a < $count_b ? -1 : 1;
        $desc = $count_a > $count_b ? -1 : 1;

        return $settings['sort_reactions'] == 'desc' ? $desc : $asc;
      }
    });

    // Render the reaction.
    return array_map(function (VoteType $vote_type) use ($settings, $results) {
      $output = [
        '#theme' => 'votingapi_reaction_item',
        '#reaction' => $vote_type->id(),
      ];

      if ($settings['show_icon']) {
        // [Legacy], for those who is overriding twig before change below were
        // added.
        $output['#icon'] = $this->getUploadedImage($vote_type);

        $output['#icon_class'] = $vote_type->getThirdPartySetting('votingapi_reaction', 'icon_class');
        $output['#icon_type'] = $vote_type->getThirdPartySetting('votingapi_reaction', 'icon_type');
        $output['#icon_alt'] = $vote_type->label();

        switch ($output['#icon_type']) {
          default:
          case 'uploaded_image':
            $output['#uploaded_image'] = $output['#icon'];
            break;

          case 'remote_image':
            $output['#remote_image'] = $vote_type->getThirdPartySetting('votingapi_reaction', 'remote_image');
            break;

          case 'html_element':
            $output['#html_element'] = $vote_type->getThirdPartySetting('votingapi_reaction', 'html_element');
            break;
        }
      }

      if ($settings['show_label']) {
        $output['#label'] = $vote_type->label();
      }

      if ($settings['show_count']) {
        $output['#count'] = $results[$vote_type->id()]['vote_sum'] ?? 0;
      }

      return $this->renderer->render($output);
    }, $reactions);
  }

  /**
   * Store reaction from session variable.
   *
   * @param \Drupal\votingapi\Entity\Vote $entity
   *   Current vote entity.
   */
  public function rememberReaction(Vote $entity) {
    $_SESSION['votingapi_reaction'][implode(':', [
      $entity->getVotedEntityId(),
      $entity->getVotedEntityType(),
      $entity->get('field_name')->value,
    ])] = $entity->id();
  }

  /**
   * Remove reaction from session variable.
   *
   * @param \Drupal\votingapi\Entity\Vote $entity
   *   Current vote entity.
   */
  public function forgetReaction(Vote $entity) {
    unset($_SESSION['votingapi_reaction'][implode(':', [
      $entity->getVotedEntityId(),
      $entity->getVotedEntityType(),
      $entity->get('field_name')->value,
    ])]);
  }

  /**
   * Restore reaction from session variable.
   *
   * @param \Drupal\votingapi\Entity\Vote $entity
   *   Current vote entity.
   *
   * @return string|null
   *   Reaction vote id based on session.
   */
  public function recallReaction(Vote $entity) {
    $key = implode(':', [
      $entity->getVotedEntityId(),
      $entity->getVotedEntityType(),
      $entity->get('field_name')->value,
    ]);

    return !empty($_SESSION['votingapi_reaction'][$key])
      ? $_SESSION['votingapi_reaction'][$key]
      : NULL;
  }

  /**
   * Return URL to reaction icon.
   *
   * @param \Drupal\votingapi\Entity\VoteType $vote_type
   *   Current vote entity.
   *
   * @return string
   *   Reaction icon url.
   */
  public function getUploadedImage(VoteType $vote_type) {
    // Module path.
    $path = implode('/', [
      $this->pathResolver->getPath('module', 'votingapi_reaction'),
      'svg',
      // Trailing slash.
      '',
    ]);

    // Fallback icon.
    $url = $path . 'reaction_noicon.svg';
    // User defined icon.
    $icon = $vote_type->getThirdPartySetting('votingapi_reaction', 'uploaded_image');
    if ($icon && $file = File::load($icon)) {
      $url = $file->getFileUri();
    }
    // Default icon.
    elseif (file_exists(DRUPAL_ROOT . "/$path" . $vote_type->id() . '.svg')) {
      $url = $path . $vote_type->id() . '.svg';
    }

    return $this->fileUrlGenerator->generateAbsoluteString($url);
  }

}
