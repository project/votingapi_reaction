<?php

/**
 * @file
 * Contains voting_reactions.install.
 */

use Drupal\votingapi_reaction\VotingApiReactionManager;

/**
 * Implements hook_install().
 */
function votingapi_reaction_install() {
  $vote_type_storage = \Drupal::service('entity_type.manager')
    ->getStorage('vote_type');

  // Create reactions.
  $vote_types = $vote_type_storage->loadMultiple(
    $vote_type_storage->getQuery()
      ->accessCheck(FALSE)
      ->condition('id', array_keys(VotingApiReactionManager::DEFAULT_REACTIONS), 'IN')
      ->execute()
  );
  foreach (VotingApiReactionManager::DEFAULT_REACTIONS as $key => $label) {
    /** @var \Drupal\votingapi\VoteTypeInterface $vote_type */
    $vote_type = empty($vote_types[$key])
      ? $vote_type_storage->create([
        'id' => $key,
        'label' => $label,
        'value_type' => 'points',
        'description' => 'Automatically created reaction',
      ])
      : $vote_types[$key];
    $vote_type->setThirdPartySetting('votingapi_reaction', 'reaction', TRUE);
    $vote_type->setThirdPartySetting('votingapi_reaction', 'icon_type', 'uploaded_image');
    $vote_type->setThirdPartySetting('votingapi_reaction', 'icon_class', '');
    $vote_type->setThirdPartySetting('votingapi_reaction', 'uploaded_image', '');
    $vote_type->setThirdPartySetting('votingapi_reaction', 'remote_image', '');
    $vote_type->setThirdPartySetting('votingapi_reaction', 'html_element', '');
    $vote_type->save();
  }
}

/**
 * Implements hook_uninstall().
 */
function votingapi_reaction_uninstall() {
  $vote_type_storage = \Drupal::service('entity_type.manager')
    ->getStorage('vote_type');

  // Remove reactions.
  $vote_types = $vote_type_storage->loadMultiple(
    $vote_type_storage->getQuery()
      ->accessCheck(FALSE)
      ->condition('id', array_keys(VotingApiReactionManager::DEFAULT_REACTIONS), 'IN')
      ->execute()
  );
  foreach ($vote_types as $vote_type) {
    /** @var \Drupal\votingapi\VoteTypeInterface $vote_type */
    $vote_type->delete();
  }
}

/**
 * Update field configs with new structure.
 */
function votingapi_reaction_update_8107(&$sandbox) {
  // Get all votingapi_reaction fields.
  /** @var \Drupal\field\Entity\FieldConfig $fields */
  $fields = \Drupal::entityTypeManager()
    ->getStorage('field_config')
    ->loadByProperties(['field_type' => 'votingapi_reaction']);
  foreach ($fields as $field) {
    // If votingapi_reaction has the right setting.
    if ($reactions = $field->getSetting('reactions')) {
      foreach ($reactions as $key => $value) {
        // Update old string based setting into array.
        if (is_string($value)) {
          $reactions[$key] = [
            'weight' => 0,
            'show' => $key === $value,
          ];
        }
      }
    }

    // Save the field settings.
    $field->setSetting('reactions', $reactions);
    $field->save();
  }
}

/**
 * Update vote types with new settings.
 */
function votingapi_reaction_update_8109(&$sandbox) {
  // Get all votingapi_reaction fields.
  /** @var \Drupal\votingapi\Entity\VoteType $vote_types */
  $vote_types = \Drupal::entityTypeManager()
    ->getStorage('vote_type')
    ->loadMultiple();
  foreach ($vote_types as $vote_type) {
    $settings = array_merge([
      'reaction' => FALSE,
      'icon_type' => 'uploaded_image',
      'icon_class' => '',
      'uploaded_image' => '',
      'remote_image' => '',
      'html_element' => '',
    ], $vote_type->getThirdPartySettings('votingapi_reaction') ?? []);

    // Move old settings to the new setting.
    if (isset($settings['icon'])) {
      $settings['uploaded_image'] = $settings['icon'];
      unset($settings['icon']);
    }

    foreach ($settings as $key => $value) {
      $vote_type->setThirdPartySetting('votingapi_reaction', $key, $value);
    }
    $vote_type->save();
  }
}

/**
 * Fix field configs.
 */
function votingapi_reaction_update_8110() {
  /** @var \Drupal\Core\Field\FieldStorageDefinitionInterface $field */
  $fields = \Drupal::entityTypeManager()
    ->getStorage('field_storage_config')
    ->loadMultiple();
  foreach ($fields as $field) {
    if ($field->getType() === 'votingapi_reaction') {
      $field->save();
    }
  }
}
